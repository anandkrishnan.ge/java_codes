
import java.util.Comparator;

public class FreieRechner implements Comparator <Laborplaner> {
	
	public int time_table() { // function to print the time-table
		int count_calculator = 0;
		int count = 0;

		if (count == 0) {
			for (int j = 0; j < 5; j++) {
				for (int k = 0; k < 5; k++) {
					count_calculator = count_calculator + 5;
				}
			}
			return count_calculator;
		} else {
			return count;
		}
	}
	
	@Override public int compare(Laborplaner labor1, Laborplaner labor2) {
		return labor2.available_slots_per_week() - labor1.available_slots_per_week();
	}
}
