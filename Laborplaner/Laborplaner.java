

public class Laborplaner implements Comparable<Laborplaner> {

	private int count;
	private int day_hours = 8;
//	private String zeilentrenner = System.getProperty("line.separator"); (If required) To get the seperator according to the operating system
	
	private String name;
	
	private int[][] laborplan;

	Laborplaner(String name, int count) {
		if (count < 0) {
			throw new IllegalArgumentException("Ungültige Anzahl: " + count);
		}

		this.count = count;
		this.laborplan = new int[10][05];
		this.name = name;

		for (int j = 0; j < 10; j++)
			for (int k = 0; k < 5; k++)
				laborplan[j][k] = 0;
	}

	int nochFrei(Tag day, int hours) { //checks for the free hours and day
		
		if (day.ordinal() == 5 || day.ordinal() == 6) {
			throw new IllegalArgumentException("Ungültiger Tag: " + day);
		}
		if (hours < 8 || hours > 17) {  
			throw new IllegalArgumentException("Ungültige Stunde: " + hours);
		}
		return this.count - laborplan[hours - day_hours][day.ordinal()];

	}


	@Override
	public String toString() { // create the first line and ammend it
		String first_line = new String();
		first_line = "";
		String days = new String();
		days = "  Mo Di Mi Do Fr\n";
		if (this.name.length() >= 5) {
			first_line = this.name.substring(0, 5) + days;
		} else {
			while (first_line.length() < 5) {
				first_line = this.name = this.name + " ";
			}
			first_line = first_line + days;
		}

		for (int j = 0; j < laborplan.length; j = j+1) {
			if (j < 2) {
				first_line = first_line + "0" + (j + 8) + ":00 ";
			}
			if (j >= 2) {
				first_line = first_line + (j + 8) + ":00 ";
			}
			for (int k = 0; k < laborplan[j].length; k = k+1) {
				if (k >= 0) {
					int belegung = laborplan[j][k];
					if (belegung >= 10) {
						first_line = first_line + " " + belegung;

						
					} else {
						first_line = first_line + "  " + belegung;

					}
				}

			}
			first_line = first_line + "\n";

		}
		return first_line.toString();

	}

	static int verhandleStunde(Tag day, int count, Laborplaner[] plan_list) {
		if (day.ordinal() == 5 || day.ordinal() == 6) {
			throw new IllegalArgumentException("Ungültiger Tag: " + day);
		}
		if (count < 0) {
			throw new IllegalArgumentException("Ungültige Anzahl: " + count);
		}

		int initial_count = 0;
		for (int i = 0; i < plan_list.length; i++) {
			Laborplaner list_position = plan_list[i];
			initial_count = initial_count + list_position.getCount();
		}
		if (initial_count < count) {
			return -1;
		}
		for (int hours = 8; hours < 17; hours++) {
			int free_hours = 0;

			for (int i = 0; i < plan_list.length; i++) {
				Laborplaner list_position = plan_list[i];
				free_hours = free_hours + list_position.nochFrei(day, hours);

			}
			if (free_hours >= count) {
				for (int i = 0; i < plan_list.length; i++) {
					Laborplaner list_position = plan_list[i];
					count = count - list_position.belege(day, hours, count);

				}
				return hours;
			}
		}
		return -1;
	}
	
	

	@Override
	public int compareTo(Laborplaner that) {
		return this.name.compareTo(that.getName());
	}

	public int avaliability_check_1() { //check the avaialability 
		int counter = 0;

		if (count == 0) {
			for (int j = 0; j < this.laborplan.length; j++) {
				for (int k = 0; k < this.laborplan[j].length; k++) {
					counter = counter + this.laborplan[j][k];
				}
			}
			return counter;
		} else {
			return count;
		}
	}

	public int getCount() {
		return count;
	}

	public String getName() {
		return name;
	}
	
	int belege(Tag day, int hours, int count) {
		if (day.ordinal() == 5 || day.ordinal() == 6) {
			throw new IllegalArgumentException("Ungültiger Tag: " + day);
		}
		if (hours < 8 || hours > 17) {
			throw new IllegalArgumentException("Ungültige Stunde: " + hours);
		}
		if (count < 0) {
			throw new IllegalArgumentException("Ungültige Anzahl: " + count);
		}
		int availabe_slots = this.count - laborplan[hours - day_hours][day.ordinal()];

		if (availabe_slots < count) {
			laborplan[hours - day_hours][day.ordinal()] = this.count;
			return availabe_slots;
		} else {
			laborplan[hours - day_hours][day.ordinal()] = laborplan[hours - day_hours][day.ordinal()] + count;
			return count;

		}
	}



	public int available_slots_per_week() {
		int counter = 0;

		if (count == 0) {
			for (int j = 0; j < this.laborplan.length; j++) {
				for (int k = 0; k < this.laborplan[j].length; k++) {
					counter = counter + this.laborplan[j][k];
				}
			}
			return counter;
		} else {
			return count;
		}
	}
	public static void main(String[] args) {
		Laborplaner b202 = new Laborplaner("B202", 18), mechatronik = new Laborplaner("Mechatronik", 12);
		b202.belege(Tag.Dienstag, 11, 11);
		b202.belege(Tag.Mittwoch, 9, 12);
		b202.belege(Tag.Freitag, 14, 20);
		mechatronik.belege(Tag.Donnerstag, 8, 6);
		mechatronik.belege(Tag.Mittwoch, 8, 25);
		mechatronik.belege(Tag.Donnerstag, 8, 10);
		System.out.println(b202);
		System.out.println(mechatronik);
	}

}
	

