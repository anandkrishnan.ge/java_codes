# java_codes

This is a gitlab project to showcase my recent project written in JAVA.


# Project : _Laborplaner_

`    This java program consists of 3 classes, ie; Laborplaner , FreieRechner & Tag .It takes in values such as day, time and number of students through the main function within the class Laborplaner and prints out the timetable/schedule for specific classrooms/Labor-Rooms during that week in a 2D array form.`

## Main implementations and understanding gained thorugh this project:

    1. Usage of comparable instance and its implementation
    2. Usage of @Override to override the base class method with a child class
    3. Usage of enum to represent constants. eg: Days of week
    4. Usage of keywords such as `throw` to show an exception within a method 
    5. Problem solving skills using JAVA and Object Oriented Programming

## *Note:
`    Since the project was to be submitted for my study program, some of the methods were supposed to be named in German, hence a mix of german and english namings used.`

## Example and output:

**Lets consider the input to be:**

```
Laborplaner b202 = new Laborplaner("B202", 18), mechatronik = new Laborplaner("Mechatronik", 12);  //Defining 2 classrooms
b202.belege(Tag.Dienstag, 11, 11); //Giving in the schedule for room B202 on Tuesday at 11 o Clock with 11 number of students.
b202.belege(Tag.Mittwoch, 9, 12);
b202.belege(Tag.Freitag, 14, 20);
mechatronik.belege(Tag.Donnerstag, 8, 6); //Schedule for Labaratory Mechatronik on Thursday at 8 o Clock with 6 number of students.
mechatronik.belege(Tag.Mittwoch, 8, 25);
mechatronik.belege(Tag.Donnerstag, 8, 10);

```

**Output for this specific timetable would be:**


```
B202   Mo Di Mi Do Fr
08:00   0  0  0  0  0
09:00   0  0 12  0  0
10:00   0  0  0  0  0
11:00   0 11  0  0  0
12:00   0  0  0  0  0
13:00   0  0  0  0  0
14:00   0  0  0  0 18
15:00   0  0  0  0  0
16:00   0  0  0  0  0
17:00   0  0  0  0  0

Mecha  Mo Di Mi Do Fr
08:00   0  0 12 12  0
09:00   0  0  0  0  0
10:00   0  0  0  0  0
11:00   0  0  0  0  0
12:00   0  0  0  0  0
13:00   0  0  0  0  0
14:00   0  0  0  0  0
15:00   0  0  0  0  0
16:00   0  0  0  0  0
17:00   0  0  0  0  0


```
